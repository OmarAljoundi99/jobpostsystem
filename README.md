# Specification 

Ruby Version: 3.0.2
Rails Version: 6.1.4

# API Documentation 

The API has a restriction on using any of the endpoints, the user
can't access any of the endpoints unless the user authorized. 

# Sign up / Log in end [endpoints]

/signup --> post method takes { name:string , email:string , password:string , password_confirmation:string , recruiter:boolean }
/auth/login ==> post method takes { email:string , password:string }


# Job seekers can only do two things about a post, either see it or apply for it. 

* /post --> get method retriev all the posts.
* /post/:post_id/jopapplications --> apply for a specific post. 

# Recruiters can see their Post Jobs, their applicants, and Modify any of their posts.

* /post --> post method take { title:string or discription:string } to create a new post for the recruiter. 
* /post --> get method to retrieve all the posts attach to the recruiter. 
* /post/:post_id --> get method retrive a specific post attach the recruiter. 
* /post/:post_id --> put method takes { title:string or discription:string } to update a specific post for the recruiter.  
* /post/:post_id --> delete method to delete a specific post for the recruiter.

* /posts/:post_id/jopapplications --> get method retrive all the Job applications that attach to the recruiter.
* /posts/:post_id/jopapplications/:jopapplicationId --> get method show detiles of the user who applied for this post. 