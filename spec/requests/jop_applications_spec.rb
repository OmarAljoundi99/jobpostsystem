require 'rails_helper'
require 'pp'
RSpec.describe 'jopapplications API', type: :request do
  let(:user) { create(:user) }
  let!(:post_user) { create(:post,user_id: user.id) }
  let!(:jop_applications) { create(:jop_application, post_id: post_user.id, user_id:user.id) }
  let(:headers) { valid_headers }

   describe 'GET /jobposts/:jobpost_id/applications' do
    before { get "/posts/#{post_user.id}/jopapplications", params: {}, headers: headers }
    it 'returns applications' do
      expect(json).not_to be_empty
      expect(json.size).to eq(1)
    end
      it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
  describe 'GET /posts/:post_id/jopapplications/:id' do
    before { get "/posts/#{post_user.id}/jopapplications/#{jop_applications.id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the application' do
        expect(json).not_to be_empty
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end
    describe 'POST /posts/:post_id/jopapplications' do
     let(:valid_attributes) do
      { jobpost_id: post_user.id.to_s }.to_json
    end

    context 'when request is valid' do
      before { post "/posts/#{post_user.id}/jopapplications", params: valid_attributes, headers: headers }

      it 'creates an application' do
        expect(response).to have_http_status(201)
      end
      end
    end
end

