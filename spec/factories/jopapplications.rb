FactoryBot.define do
  factory :jop_application do
    status { false }
    user_id { nil }
    post_id { nil }
  end
end