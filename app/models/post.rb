class Post < ApplicationRecord
  belongs_to :user
  has_many :jop_applications,dependent: :destroy, foreign_key: :post_id

  validates_presence_of :title, :description
end
