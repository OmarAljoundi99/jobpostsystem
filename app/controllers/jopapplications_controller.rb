class JopapplicationsController < ApplicationController

  def create 
    if current_user.recruiter === false
    @post = Post.find_by(id: params[:post_id])
      if @post.present?              
        @application = current_user.jop_applications.create!(user_id:@current_user.id,post_id:params[:post_id],status:false)
        render json: {data: @application}, status: :created
      else
        render json: {message: "Post is not exist"}, status: :not_found
      end  
      else
        render json: {message: "Not Authorized"}, status: :unauthorized
      end
  end
  def show 
    if current_user.recruiter === true
    @post = Post.find_by(id: params[:post_id])
      if @post.present?              
        @application = JopApplication.find_by(post_id: @post.id,id: params[:id])
        @user = User.find_by(id: @application.user_id)
        @application.status = true
        @application.save
        render json: {id:@user.id ,Email:@user.email, Name:@user.name}, status: :ok
      else
        render json: {message: "Post is not exist"}, status: :not_found
      end 
    else   
    render json: {message: "Not Authorized"}, status: :unauthorized
    end
  end

  def index
    if current_user.recruiter === true
    @post = Post.find_by(id: params[:post_id],user_id: current_user.id)
      if @post.present?
        @applications = JopApplication.select { |app| app.post_id === @post.id }
        if @applications.present?
          json_response(@applications)
        else
          json_response("Couldn't find Applications")
        end
      else 
        json_response("No Posts Found")
      end
    else
      render json: {message: "Not Authorized"}, status: :unauthorized
    end
 end
end

