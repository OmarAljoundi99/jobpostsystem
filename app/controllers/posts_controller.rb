class PostsController < ApplicationController
 before_action :set_post, only: [:show, :update, :destroy]

  def index
    if current_user.recruiter === false
      json_response(Post.all)
    else
       json_response(current_user.posts)
     end
  end

  def show
    json_response(@post)
  end

  def create
    if current_user.recruiter === true
      @post = current_user.posts.create!(post_params)
      json_response(@post, :created)
    else
      render json: { message: "Not Authorized" }, status: :unauthorized
    end
  end

  def update
    if current_user.recruiter === true && current_user.posts.find_by(id: params[:id])
      @post = current_user.posts.update(post_params)
      render json: {message: "Post Has Been Updated"}, status: :no_content
    else
      render json: {message: "Not Authorized"}, status: :unauthorized
    end
  end


   def destroy
    if current_user.recruiter === true && current_user.posts.find_by(id: params[:id])
      @post = current_user.posts.find_by(id: params[:id])
      @post.destroy
      render json: {message: "Post Has Been Deleted "}, status: :no_content
    else
      render json: {message: "Not Authorized "}, status: :unauthorized
    end
  end

  private

  def post_params
    params.permit(:title,:description)
  end

  def set_post
    @post = Post.find(params[:id])
  end
end

