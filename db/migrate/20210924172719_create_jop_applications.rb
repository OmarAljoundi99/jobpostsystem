class CreateJopApplications < ActiveRecord::Migration[6.1]
  def change
    create_table :jop_applications do |t|
      t.references :user, foreign_key: true
      t.references :post, foreign_key: true
      t.boolean :status

      t.timestamps
    end
  end
end
